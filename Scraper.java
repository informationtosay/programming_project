package programming;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Scraper {

    public static void main(String[] args) throws Exception {

        final Document document = Jsoup.connect("https://www.imdb.com/chart/top/?ref_=nv_mv_250").get();
        Elements ratingAndViewsElements = doc.getElementsByClass("ipc-rating-star ipc-rating-star--base ipc-rating-star--imdb ratingGroup--imdb-rating");
        int a = 0;


        for (Element row : document.select(".ipc-metadata-list-summary-item__c")) {

            final String title = row.select(".ipc-title__text").text();
            
            Element ratingAndViews = ratingAndViewsElements.get(a);
            String ratAndView = ratingAndViews.text();
            String[] rating = ratAndView.split(" ");
            
            System.out.println(title + " -> Rating: " + rating);
        }
    }
}
