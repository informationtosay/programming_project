package programming;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.List;

public class IMDBScraper {
    public static void main(String[] args) {
        // Set the path to your ChromeDriver executable
        System.setProperty("webdriver.chrome.driver", "/path/to/chromedriver");
        
        // Set Chrome options for running in headless mode
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless"); // Run in headless mode
        
        // Initialize ChromeDriver with headless options
        WebDriver driver = new ChromeDriver(options);
        
        // Navigate to IMDb website
        driver.get("https://www.imdb.com/chart/top/");
        
        // Find all movie elements
        List<WebElement> movieElements = driver.findElements(By.cssSelector("tbody.lister-list tr"));
        
        // Iterate over each movie element
        for (WebElement movieElement : movieElements) {
            // Extract movie title
            String title = movieElement.findElement(By.cssSelector(".titleColumn a")).getText();
            
            // Extract movie rating
            String rating = movieElement.findElement(By.cssSelector(".imdbRating strong")).getText();
            
            // Print movie title and rating
            System.out.println("Title: " + title + ", Rating: " + rating);
        }
        
        // Close the WebDriver
        driver.quit();
    }
}
