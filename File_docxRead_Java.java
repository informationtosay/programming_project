package programming;

import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class File_docxRead_Java {

	public static void main(String[] args) 
    { 
  
		 try {
	            // Specify the path to your .docx file
	            String filePath = "/home/ramu/Downloads/Task_docx_File.docx";
	            
	            // Open the .docx file
	            FileInputStream fis = new FileInputStream(filePath);
	            XWPFDocument docx = new XWPFDocument(fis);
	            
	            // Get the text from the document
	            XWPFWordExtractor extractor = new XWPFWordExtractor(docx);
	            String text = extractor.getText();
	            
	            // Print the extracted text
	            System.out.println(text);
	            
	            // Close the input stream
	            fis.close();
	        } catch (IOException e) {
	            e.printStackTrace();
    } 

}
}
